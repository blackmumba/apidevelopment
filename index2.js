/* import the 'restify' module and create an instance. */
var restify = require('restify')
var server = restify.createServer()

/* import the required plugins to parse the body and auth header. */
server.use(restify.fullResponse())
server.use(restify.bodyParser())
server.use(restify.authorizationParser())


/* import our custom module. */
var trivia = require('./user.js')

/* if we receive a GET request for the base URL redirect to /lists */
server.get('/', function(req, res, next) {
  res.redirect('/dictionary', next)
})

/* This route provides a URL for each list resource. It includes a parameter (indicated by a :). The string entered here is stored in the req.params object and can be used by the script. */
server.get('/dictionary', function(req, res) {
  
  /* Here we store the id we want to retrieve in an 'immutable variable'. */
  const dictionaryID = req.params.query;
  
  
  /* Notice that all the business logic is kept in the 'lists' module. This stops the route file from becoming cluttered and allows us to implement 'unit testing' (we cover this in a later topic) */
  const data = dictionaryID.search(dictionaryID, function(data) {
  res.setHeader('content-type', 'application/json')
  res.send(data.code, data.response)
  
  res.end()
})

})

var port = process.env.PORT || 8080;
server.listen(port, function (err) {
  if (err) {
      console.error(err);
  } else {
    console.log('App is ready at : ' + port);
  }
})