var reader = require('fs')
var restify = require('restify')
require("./database.js")
var server = restify.createServer()


server.use(restify.fullResponse())
server.use(restify.bodyParser())
server.use(restify.authorizationParser())

var manga = require ('./manga.js')

/*if the get recives a request for the base URL redirect to the list*/
server.get('/manga',manga.AllManga)
server.get('/manga/:id',manga.Mangaid)

var port = process.env.PORT;
server.listen(port, function (err) {
  if (err)
    console.error(err)
  else
    console.log('Server is now running')
})